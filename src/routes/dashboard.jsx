import Dashboard from "views/Dashboard/Dashboard";
import UserProfile from "views/UserProfile/UserProfile";
import TableList from "views/TableList/TableList";
import Typography from "views/Typography/Typography";
import Icons from "views/Icons/Icons";
import Maps from "views/Maps/Maps";
import Notifications from "views/Notifications/Notifications";
import Upgrade from "views/Upgrade/Upgrade";

const dashboardRoutes = [
  {
    path: "/user",
    name: "Insert User",
    icon: "pe-7s-user",
    component: UserProfile
  },
  {
    path: "/phone",
    name: "Insert Phone",
    icon: "pe-7s-phone",
    component: UserProfile
  },

  {
    path: "/viewuser",
    name: "View Users",
    icon: "pe-7s-user",
    component: UserProfile
  },
  {
    path: "/viewphone",
    name: "View Phones",
    icon: "pe-7s-phone",
    component: UserProfile
  },
  {
    path: "/viewphone",
    name: "Schedule Delivery",
    icon: "pe-7s-timer",
    component: UserProfile
  },
  { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
];

export default dashboardRoutes;
